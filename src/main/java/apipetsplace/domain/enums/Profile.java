package apipetsplace.domain.enums;

public enum Profile {

    ADMIN(1, "ROLE_ADMIN"),
    CONSUMER(2, "ROLE_CONSUMER");

    private int id;
    private String description;

    private Profile(final int id, final String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public static Profile toEnum(final Integer id) {
        if (id == null) {
            return null;
        }

        for (final Profile x : Profile.values()) {
            if (id.equals(x.getId())) {
                return x;
            }
        }
        throw new IllegalArgumentException("Invalid ID: " + id);
    }


}
