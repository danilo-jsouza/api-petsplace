package apipetsplace.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "orders")
public final class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Instant createdDate; // TODO Talvez ao invés de Date salvar como Timestamp?

    @ManyToOne
    private Consumer consumer;

    @ManyToOne
    private Destiny destiny;

    @OneToMany(mappedBy = "id.order")
    private Set<OrderItem> orderItems = new HashSet<>();

    public Order() {
    }

    public Order(final Integer id, final Instant createdDate, final Consumer consumer, final Destiny destiny) {
        this.id = id;
        this.createdDate = createdDate;
        this.consumer = consumer;
        this.destiny = destiny;
    }

    public double getTotalValue() {
        double sum = 0.0;
        for (OrderItem orderItem : orderItems) {
            sum = sum + orderItem.getSubtotal();
        }
        return sum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(final Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(final Consumer consumer) {
        this.consumer = consumer;
    }

    public Destiny getDestiny() {
        return destiny;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(final Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public void setDestiny(final Destiny destiny) {
        this.destiny = destiny;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

