package apipetsplace.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonInputMessage;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;

@Component
public class JsonConverter {

    private static class StringHttpOutputMessage implements HttpOutputMessage {

        private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

        private final HttpHeaders headers = new HttpHeaders();

        private final ByteArrayOutputStream body = new ByteArrayOutputStream(1024);


        /**
         * Return the headers.
         */
        @Override
        public HttpHeaders getHeaders() {
            return this.headers;
        }

        /**
         * Return the body content.
         */
        @Override
        public OutputStream getBody() throws IOException {
            return this.body;
        }

        /**
         * Return body content as a byte array.
         */
        public byte[] getBodyAsBytes() {
            return this.body.toByteArray();
        }

        /**
         * Return the body content interpreted as a UTF-8 string.
         */
        public String getBodyAsString() {
            return getBodyAsString(DEFAULT_CHARSET);
        }

        /**
         * Return the body content as a string.
         *
         * @param charset the charset to use to turn the body content to a String
         */
        public String getBodyAsString(final Charset charset) {
            final byte[] bytes = getBodyAsBytes();
            try {
                return new String(bytes, charset.name());
            } catch (final UnsupportedEncodingException ex) {
                // should not occur
                throw new IllegalStateException(ex);
            }
        }

    }

    private static Logger logger = LoggerFactory.getLogger(JsonConverter.class);

    private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

    protected final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    void setConverters(final HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = (MappingJackson2HttpMessageConverter) Arrays.asList(converters)
                .stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .get();
        Objects.requireNonNull(this.mappingJackson2HttpMessageConverter, "the JSON message converter must not be null");

        this.mappingJackson2HttpMessageConverter.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public Function<Object, String> toJsonFunction() {
        return (object) -> toJson(object);
    }

    public <T> Function<String, T> fromJsonFunction(final Class<T> clazz) {
        return (json) -> {
            try {
                return toObject(json, clazz);
            } catch (IOException | HttpMessageNotReadableException e) {
                throw new RuntimeException("Couldn't parse json " + json, e);
            }
        };
    }

    public String toJson(final Object o) {
        final StringHttpOutputMessage httpOutputMessage = new StringHttpOutputMessage();
        try {
            mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, httpOutputMessage);
        } catch (HttpMessageNotWritableException | IOException e) {
            logger.error("Error to convert to json", e);
            throw Throwables.propagate(e);
        }
        return httpOutputMessage.getBodyAsString();
    }

    public <T> T toObject(final String json, final Class<? extends T> clazz) throws HttpMessageNotReadableException,
            IOException {
        if (Strings.isNullOrEmpty(json)) {
            return null;
        }
        final MappingJacksonInputMessage inputMessages = new MappingJacksonInputMessage(
                new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)), new HttpHeaders());
        return (T) mappingJackson2HttpMessageConverter.read(clazz, inputMessages);
    }

    public <T> T toObject(final Map<String, Object> json, final Class<? extends T> clazz) throws HttpMessageNotReadableException {

        if (json == null) {
            return null;
        }

        return (T) mappingJackson2HttpMessageConverter.getObjectMapper().convertValue(json, clazz);
    }

    /**
     * Converts a given json String to a {@link Collection} object.
     *
     * @param json            the json itself
     * @param collectionClass the type of the collection
     * @param elementClass    the type of the collection elements
     * @return <code>null</code> if ths json is null or empty or an instance of a collection of type
     * <code>C</code> containing instances of type <code>E</code>
     * @throws IOException when parsing the json fails
     */
    public <C, E> C toCollectionObject(final String json, final Class<? extends C> collectionClass,
                                       final Class<? extends E> elementClass) throws IOException {
        if (Strings.isNullOrEmpty(json)) {
            return null;
        }

        final byte[] src = json.getBytes(StandardCharsets.UTF_8);
        final ObjectMapper mapper = mappingJackson2HttpMessageConverter.getObjectMapper();
        return mapper.readValue(src,
                mapper.getTypeFactory().constructCollectionLikeType(collectionClass, elementClass));
    }

    public <C, E> C toCollectionObject(final String json, final String fieldName,
                                       final Class<? extends C> collectionClass,
                                       final Class<? extends E> elementClass) throws IOException {
        return toCollectionObject(json, collectionClass, elementClass, fieldName);
    }

    public <C, E> C toCollectionObject(final String json, final Class<? extends C> collectionClass,
                                       final Class<? extends E> elementClass,
                                       final String... fieldNames) throws IOException {

        final ObjectMapper mapper = mappingJackson2HttpMessageConverter.getObjectMapper();
        JsonNode node = null;
        for (final String fieldName : fieldNames) {
            if (node == null) {
                node = mapper.readTree(json).get(fieldName);
            } else {
                node = node.get(fieldName);
            }
        }
        return toCollectionObject(node.toString(), collectionClass, elementClass);
    }

    public <K, V> Map<K, V> toMapObject(final String json, final Class<K> keyClass,
                                        final Class<V> elementClass) throws IOException {
        if (Strings.isNullOrEmpty(json)) {
            return null;
        }

        final byte[] src = json.getBytes(StandardCharsets.UTF_8);
        final ObjectMapper mapper = mappingJackson2HttpMessageConverter.getObjectMapper();
        return mapper.readValue(src,
                mapper.getTypeFactory().constructMapLikeType(HashMap.class, keyClass, elementClass));
    }

    public <K, E, C extends Collection<E>> Map<K, C> toMapCollectionValuesObject(
            final String json,
            final Class<K> keyClass,
            final Class<C> elementCollectionClass,
            final Class<E> collectionElementClass) throws IOException {
        if (Strings.isNullOrEmpty(json)) {
            return null;
        }

        final byte[] src = json.getBytes(StandardCharsets.UTF_8);
        final ObjectMapper mapper = mappingJackson2HttpMessageConverter.getObjectMapper();
        final TypeFactory typeFactory = mapper.getTypeFactory();
        return mapper.readValue(src,
                typeFactory.constructMapType(HashMap.class,
                        typeFactory.constructType(keyClass),
                        typeFactory.constructCollectionType(elementCollectionClass, collectionElementClass)));
    }

    /**
     * Configures a new Jackson {@link ObjectMapper} and returns it.
     *
     * @return a new and configured Jackson {@link ObjectMapper}
     */
    public ObjectMapper configureObjectMapper() {
        return mappingJackson2HttpMessageConverter.getObjectMapper().copy();
    }
}
