package apipetsplace.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class URL {

    public static String decodeParameters(final String parameterString) {
        try {
            return URLDecoder.decode(parameterString, "UTF-8");
        } catch (UnsupportedEncodingException encodingException) {
            return "";
        }
    }

    public static List<Integer> decodeIntList(final String parametersURL) {
        return Arrays.asList(parametersURL.split(",")).stream().map(Integer::parseInt).collect(Collectors.toList());
    }

}
