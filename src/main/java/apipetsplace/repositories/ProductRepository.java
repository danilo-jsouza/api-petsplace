package apipetsplace.repositories;

import apipetsplace.domain.Category;
import apipetsplace.domain.Product;
import apipetsplace.domain.SubCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT product " +
            "FROM Product product " +
            "INNER JOIN product.subCategory sub " +
            "INNER JOIN sub.category cat " +
            "WHERE cat.id = :id ")
    Page<Product> searchProductByCategories(@Param("id") Integer id,
                                               Pageable pageRequest);

    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT product " +
            "FROM Product product " +
            "INNER JOIN product.subCategory sub WHERE sub IN :subCategory ")
    Page<Product> searchProductBySubCategories(@Param("subCategory") List<SubCategory> subCategories,
                                               Pageable pageRequest);

    @Query("SELECT DISTINCT product " +
            "FROM Product product " +
            "WHERE product.name LIKE %:name%")
    Page<Product> searchProductByName(@Param("name") String name, Pageable pageRequest);


    List<Product> findAllByOrderByIdAsc();


}
