package apipetsplace.repositories;

import apipetsplace.domain.Consumer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ConsumerRepository extends JpaRepository<Consumer, Integer> {

    @Transactional(readOnly = true)
    Consumer findByEmail(String email);

}
