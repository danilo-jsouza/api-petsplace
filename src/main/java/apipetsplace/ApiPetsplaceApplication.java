package apipetsplace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPetsplaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPetsplaceApplication.class, args);
	}

}
