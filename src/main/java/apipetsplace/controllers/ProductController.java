package apipetsplace.controllers;

import apipetsplace.domain.Product;
import apipetsplace.dto.NewProductDTO;
import apipetsplace.dto.ProductDTO;
import apipetsplace.services.ProductService;
import apipetsplace.utils.URL;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productServices) {
        this.productService = Objects.requireNonNull(productServices);
    }

    @GetMapping(value = "/{productId}")
    public ResponseEntity<ProductDTO> getProduct(@PathVariable final Integer productId) {
        Product product = productService.findOne(productId);
        ProductDTO productDTO = new ProductDTO(product);
        return ResponseEntity.ok().body(productDTO);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<Product> insert(@Valid @RequestBody final NewProductDTO newProductDTO) {
        Product product = productService.fromDTO(newProductDTO);
        product = productService.insert(product);
        return ResponseEntity.ok().body(product);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody final NewProductDTO productDTO,
                                    @PathVariable final Integer id) {
        Product product = productService.fromDTO(productDTO);
        productService.update(product);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<ProductDTO>> findAll() {
        List<Product> productList = productService.findAll();
        List<ProductDTO> productDTO = productList
                .stream()
                .map(ProductDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(productDTO);
    }

    @GetMapping(value = "/pageable/filter/category")
    public ResponseEntity<Page<ProductDTO>> findProductsByCategory(
            @RequestParam(value = "categoryId", required = false) final Integer categoryId,
            @RequestParam(value = "page", defaultValue = "0") final Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") final Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "name") final String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") final String direction) {

        final Page<Product> list = productService.searchProductByCategory(categoryId, page, linesPerPage, orderBy, direction);
        final Page<ProductDTO> productDTOS = list.map(ProductDTO::new);
        return ResponseEntity.ok().body(productDTOS);
    }


    @GetMapping(value = "/pageable/filter/subCategories")
    public ResponseEntity<Page<ProductDTO>> findProductsBySubCategories(
            @RequestParam(value = "subCategoriesIds", required = false) final String subCategoriesIds,
            @RequestParam(value = "page", defaultValue = "0") final Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") final Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "name") final String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") final String direction) {
        final List<Integer> ids = Optional.of(URL.decodeIntList(subCategoriesIds)).orElse(null);
        final Page<Product> list = productService.searchProductBySubCategoriesId(ids, page, linesPerPage, orderBy, direction);
        final Page<ProductDTO> productDTOS = list.map(ProductDTO::new);
        return ResponseEntity.ok().body(productDTOS);
    }

    @GetMapping(value = "/pageable/filter/name")
    public ResponseEntity<Page<ProductDTO>> findProductsByName(
            @RequestParam(value = "productName", required = false) final String productName,
            @RequestParam(value = "page", defaultValue = "0") final Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") final Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "name") final String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") final String direction) {
        final String nameDecoded = Optional.of(URL.decodeParameters(productName)).orElse(null);
        final Page<Product> list = productService.searchProductByName(nameDecoded, page, linesPerPage, orderBy, direction);
        final Page<ProductDTO> productDTOS = list.map(ProductDTO::new);
        return ResponseEntity.ok().body(productDTOS);
    }


    @GetMapping(value = "/pageable")
    public ResponseEntity<Page<ProductDTO>> listProductsByPage(
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "linesPerPage") final Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "name") final String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") final String direction) {
        final Page<Product> list = productService.searchProduct(page, linesPerPage, orderBy, direction);
        final Page<ProductDTO> productDTOS = list.map(ProductDTO::new);
        return ResponseEntity.ok().body(productDTOS);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value = "/picture/{productId}")
    public ResponseEntity<Void> uploadProfilePicture(@RequestParam(name = "file") MultipartFile multipartFile,
                                                     @PathVariable final Integer productId) throws IOException {
        final URI uri = productService.uploadProfilePicture(multipartFile, productId);
        return ResponseEntity.created(uri).build();
    }

}
