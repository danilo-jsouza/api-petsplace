package apipetsplace.controllers;

import apipetsplace.domain.Order;
import apipetsplace.domain.Product;
import apipetsplace.dto.OrderDTO;
import apipetsplace.dto.ProductDTO;
import apipetsplace.services.OrderService;
import apipetsplace.utils.URL;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping(value = "/api/orders")
public class OrderController {


    private final OrderService orderService;

    public OrderController(OrderService orderServices) {
        this.orderService = Objects.requireNonNull(orderServices);
    }

    @GetMapping(value = "/{orderId}")
    public ResponseEntity<Order> getOrder(@PathVariable final Integer orderId) {
        Order order = orderService.findOne(orderId);
        return ResponseEntity.ok().body(order);
    }

    @PreAuthorize("hasAnyRole('CONSUMER')")
    @PostMapping
    public ResponseEntity<Order> insert(@Valid @RequestBody final OrderDTO orderDTO) {
        Order order = orderService.fromDTO(orderDTO);
        order = orderService.insert(order);
        return ResponseEntity.ok().body(order);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<Order> update(@Valid @RequestBody final OrderDTO orderDTO,
                                        @PathVariable final Integer id) {
        Order order = orderService.fromDTO(orderDTO);
        order = orderService.update(order);
        return ResponseEntity.ok().body(order);
    }

    @PreAuthorize("hasAnyRole('CONSUMER')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<Order>> findPage(
            @RequestParam(value = "page", defaultValue = "0") final Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") final Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "createdDate") final String orderBy,
            @RequestParam(value = "direction", defaultValue = "DESC") final String direction) {
        final Page<Order> orders = orderService.searchOrders(page, linesPerPage, orderBy, direction);
        return ResponseEntity.ok().body(orders);
    }

}
