package apipetsplace.controllers;


import apipetsplace.domain.Category;
import apipetsplace.dto.CategoryDTO;
import apipetsplace.services.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/categories")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryServices) {
        this.categoryService = Objects.requireNonNull(categoryServices);
    }

    @GetMapping(value = "/{categoryId}")
    public ResponseEntity<Category> getCategory(@PathVariable final Integer categoryId) {
        Category category = categoryService.findOne(categoryId);
        return ResponseEntity.ok().body(category);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<Category> insert(@Valid @RequestBody final CategoryDTO categoryDTO) {
        Category category = categoryService.fromDTO(categoryDTO);
        category = categoryService.insert(category);
        return ResponseEntity.ok().body(category);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<Category> update(@Valid @RequestBody final CategoryDTO categoryDTO,
                                           @PathVariable final Integer id) {
        Category category = categoryService.fromDTO(categoryDTO);
        category = categoryService.update(category);
        return ResponseEntity.ok().body(category);
    }

    @GetMapping
    public ResponseEntity<List<CategoryDTO>>findAll(){
        List<Category> categoryList = categoryService.findAll();
        List<CategoryDTO> categoryDTO = categoryList
                .stream()
                .map(CategoryDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(categoryDTO);
    }

}
