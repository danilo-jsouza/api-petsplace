package apipetsplace.controllers;


import apipetsplace.domain.Consumer;
import apipetsplace.dto.ConsumerDTO;
import apipetsplace.services.ConsumerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api/consumers")
public class ConsumerController {

    private final ConsumerService consumerService;

    public ConsumerController(ConsumerService consumerServices) {
        this.consumerService = Objects.requireNonNull(consumerServices);
    }

    @GetMapping(value = "/{consumerId}")
    public ResponseEntity<Consumer> getConsumer(@PathVariable final Integer consumerId) {
        Consumer consumer = consumerService.findOne(consumerId);
        return ResponseEntity.ok().body(consumer);
    }

    @PostMapping
    public ResponseEntity<Consumer> insert(@Valid @RequestBody final ConsumerDTO consumerDTO) {
        Consumer consumer = consumerService.fromDTO(consumerDTO);
        consumer = consumerService.insert(consumer);
        return ResponseEntity.ok().body(consumer);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Consumer> update(@Valid @RequestBody final ConsumerDTO consumerDTO,
                                           @PathVariable final Integer id) {
        Consumer consumer = consumerService.fromDTO(consumerDTO);
        consumer = consumerService.update(consumer);
        return ResponseEntity.ok().body(consumer);
    }

}
