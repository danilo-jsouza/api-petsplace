package apipetsplace.controllers;

import apipetsplace.domain.SubCategory;
import apipetsplace.dto.SubCategoryDTO;
import apipetsplace.services.SubCategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/categories/sub")
public class SubCategoryController {

    private final SubCategoryService subCategoryService;

    public SubCategoryController(final SubCategoryService subCategoryService) {
        this.subCategoryService = Objects.requireNonNull(subCategoryService);
    }

    @GetMapping(value = "/{subCategoryId}")
    public ResponseEntity<SubCategory> getSubCategory(@PathVariable final Integer subCategoryId) {
        SubCategory subCategory = subCategoryService.findOne(subCategoryId);
        return ResponseEntity.ok().body(subCategory);
    }

    @PostMapping
    public ResponseEntity<SubCategory> insert(@Valid @RequestBody final SubCategoryDTO subCategoryDTO) {
        SubCategory subCategory = subCategoryService.fromDTO(subCategoryDTO);
        subCategory = subCategoryService.insert(subCategory);
        return ResponseEntity.ok().body(subCategory);
    }

    @GetMapping
    public ResponseEntity<List<SubCategoryDTO>> findAll() {
        List<SubCategory> subCategoryList = subCategoryService.findAll();
        List<SubCategoryDTO> subCategoryDTO = subCategoryList
                .stream()
                .map(SubCategoryDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(subCategoryDTO);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<SubCategory> update(@Valid @RequestBody final SubCategoryDTO subCategoryDTO,
                                              @PathVariable final Integer id) {
        SubCategory subCategory = subCategoryService.fromDTO(subCategoryDTO);
        subCategory = subCategoryService.update(subCategory);
        return ResponseEntity.ok().body(subCategory);
    }

}
