package apipetsplace.controllers;

import apipetsplace.domain.Destiny;
import apipetsplace.dto.DestinyDTO;
import apipetsplace.services.DestinyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api/destinies")
public class DestinyController {

    private final DestinyService destinyService;

    public DestinyController(DestinyService destinyServices) {
        this.destinyService = Objects.requireNonNull(destinyServices);
    }

    @GetMapping(value = "/{destinyId}")
    public ResponseEntity<Destiny> getDestiny(@PathVariable final Integer destinyId) {
        Destiny Destiny = destinyService.findOne(destinyId);
        return ResponseEntity.ok().body(Destiny);
    }

    @PostMapping
    public ResponseEntity<Destiny> insert(@Valid @RequestBody final DestinyDTO destinyDTO) {
        Destiny destiny = destinyService.fromDTO(destinyDTO);
        destiny = destinyService.insert(destiny);
        return ResponseEntity.ok().body(destiny);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Destiny> update(@Valid @RequestBody final DestinyDTO destinyDTO,
                                          @PathVariable final Integer id) {
        Destiny destiny = destinyService.fromDTO(destinyDTO);
        destiny = destinyService.update(destiny);
        return ResponseEntity.ok().body(destiny);
    }

}
