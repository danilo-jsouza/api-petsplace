package apipetsplace.controllers;

import apipetsplace.security.JWTUtil;
import apipetsplace.security.UserSS;
import apipetsplace.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Objects;


@RestController
@RequestMapping(value = "/auth")
public class AuthResource {

    private final JWTUtil jwtUtil;

    public AuthResource(final JWTUtil jwtUtil) {
        this.jwtUtil = Objects.requireNonNull(jwtUtil);
    }


    @PostMapping(value = "/refresh_token")
    public ResponseEntity<Void> refreshToken(final HttpServletResponse response) {
        final UserSS user = UserService.authenticated();
        final String token = jwtUtil.generateToken(user.getUsername());
        response.addHeader("Authorization", "Bearer " + token);
        response.addHeader("access-control-expose-headers", "Authorization");
        return ResponseEntity.noContent().build();
    }

}
