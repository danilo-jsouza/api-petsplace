package apipetsplace.controllers;

import apipetsplace.domain.Brand;
import apipetsplace.dto.BrandDTO;
import apipetsplace.services.BrandService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/products/brand")
public class BrandController {

    private final BrandService brandService;

    public BrandController(BrandService brandServices) {
        this.brandService = Objects.requireNonNull(brandServices);
    }

    @GetMapping(value = "/{brandId}")
    public ResponseEntity<Brand> getBrand(@PathVariable final Integer brandId) {
        Brand brand = brandService.findOne(brandId);
        return ResponseEntity.ok().body(brand);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<Brand> insert(@Valid @RequestBody final BrandDTO brandDTO) {
        Brand brand = brandService.fromDTO(brandDTO);
        brand = brandService.insert(brand);
        return ResponseEntity.ok().body(brand);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<Brand> update(@Valid @RequestBody final BrandDTO brandDTO,
                                        @PathVariable final Integer id) {
        Brand brand = brandService.fromDTO(brandDTO);
        brand = brandService.update(brand);
        return ResponseEntity.ok().body(brand);
    }

    @GetMapping
    public ResponseEntity<List<BrandDTO>> findAll() {
        List<Brand> brandList = brandService.findAll();
        List<BrandDTO> brandDTO = brandList
                .stream()
                .map(BrandDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(brandDTO);
    }

}
