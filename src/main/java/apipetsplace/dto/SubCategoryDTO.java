package apipetsplace.dto;

import apipetsplace.domain.SubCategory;

public class SubCategoryDTO {

    private Integer id;
    private String name;
    private Integer categoryId;

    public SubCategoryDTO() {
    }

    public SubCategoryDTO(SubCategory subCategory) {
        id = subCategory.getId();
        name = subCategory.getName();
        categoryId = subCategory.getCategory().getId();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
