package apipetsplace.dto;

import apipetsplace.domain.Order;
import apipetsplace.domain.OrderItem;

import java.time.Instant;
import java.util.*;

public class OrderDTO {

    private Integer id;
    private Instant date;

    private Integer consumerId;
    private Integer destinyId;

    private Set<OrderItem> orderItems = new HashSet<>();

    public OrderDTO() {
    }

    public OrderDTO(final Order order) {
        id = order.getId();
        date = order.getCreatedDate();
        consumerId = order.getConsumer().getId();
        destinyId = order.getDestiny().getId();
        orderItems = order.getOrderItems();
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(final Instant date) {
        this.date = date;
    }

    public Integer getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(final Integer consumerId) {
        this.consumerId = consumerId;
    }

    public Integer getDestinyId() {
        return destinyId;
    }

    public void setDestinyId(final Integer destinyId) {
        this.destinyId = destinyId;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(final Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}
