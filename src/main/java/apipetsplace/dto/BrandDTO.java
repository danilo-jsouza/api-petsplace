package apipetsplace.dto;

import apipetsplace.domain.Brand;

import javax.validation.constraints.NotEmpty;

public class BrandDTO {

    private Integer id;
    @NotEmpty
    private String name;

    public BrandDTO() {
    }

    public BrandDTO(final Brand brand) {
        id = brand.getId();
        name = brand.getName();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
