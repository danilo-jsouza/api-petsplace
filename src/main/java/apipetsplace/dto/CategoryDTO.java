package apipetsplace.dto;

import apipetsplace.domain.Category;
import apipetsplace.domain.SubCategory;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

public class CategoryDTO {

    private Integer id;
    @NotEmpty
    private String name;
    private List<SubCategory> subCategories = new ArrayList<>();

    public CategoryDTO() {
    }

    public CategoryDTO(Category category) {
        id = category.getId();
        name = category.getName();
        subCategories = category.getSubCategory();
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(final List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }
}
