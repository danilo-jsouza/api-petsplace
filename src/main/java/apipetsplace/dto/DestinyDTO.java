package apipetsplace.dto;

import apipetsplace.domain.Destiny;

public class DestinyDTO {

    private Integer id;
    private String address;
    private String number;
    private String complement;
    private String neighborhood;
    private String zipcode;
    private String uf;
    private Integer consumerId;

    public DestinyDTO() {
    }

    public DestinyDTO(Destiny destiny) {
        id = destiny.getId();
        address = destiny.getAddress();
        number = destiny.getNumber();
        complement = destiny.getComplement();
        neighborhood = destiny.getNeighborhood();
        zipcode = destiny.getZipcode();
        consumerId = destiny.getConsumer().getId();
        uf = destiny.getUf();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Integer getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(final Integer consumerId) {
        this.consumerId = consumerId;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(final String uf) {
        this.uf = uf;
    }
}
