package apipetsplace.dto;

import apipetsplace.domain.Consumer;
import apipetsplace.domain.Destiny;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

public class ConsumerDTO {

    private Integer id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String email;
    @NotEmpty
    private String password;
    private List<Destiny> destinies = new ArrayList<>();

    public ConsumerDTO() {
    }

    public ConsumerDTO(final Consumer consumer) {
        id = consumer.getId();
        name = consumer.getName();
        email = consumer.getEmail();
        destinies = consumer.getDestinies();
        password = consumer.getPassword();
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public List<Destiny> getDestinies() {
        return destinies;
    }

    public void setDestinies(final List<Destiny> destinies) {
        this.destinies = destinies;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
