package apipetsplace.dto;

import apipetsplace.domain.Product;

import javax.validation.constraints.NotEmpty;

public class NewProductDTO {

    private Integer id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String description;
    @NotEmpty
    private String price;
    @NotEmpty
    private String brandId;
    @NotEmpty
    private String subCategoryId;

    private String imageURL;

    public NewProductDTO() {
    }

    public NewProductDTO(final Product product) {
        id = product.getId();
        name = product.getName();
        description = product.getDescription();
        price = String.valueOf(product.getPrice());
        brandId = String.valueOf(product.getBrand().getId());
        subCategoryId = String.valueOf(product.getSubCategory().getId());
        imageURL = product.getProductImageURL();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(final String price) {
        this.price = price;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(final String brandId) {
        this.brandId = brandId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(final String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(final String imageURL) {
        this.imageURL = imageURL;
    }
}
