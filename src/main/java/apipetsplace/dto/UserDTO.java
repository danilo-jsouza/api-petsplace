package apipetsplace.dto;


import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class UserDTO {

    private Integer id;
    private String email;
    private String token;
    final Collection<? extends GrantedAuthority> profiles;

    public UserDTO(final Integer id, final String email, final String token, Collection<? extends GrantedAuthority> profiles) {
        this.id = id;
        this.email = email;
        this.token = token;
        this.profiles = profiles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Collection<? extends GrantedAuthority> getProfiles() {
        return profiles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }
}
