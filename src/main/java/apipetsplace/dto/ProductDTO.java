package apipetsplace.dto;


import apipetsplace.domain.Brand;
import apipetsplace.domain.Category;
import apipetsplace.domain.Product;
import apipetsplace.domain.SubCategory;

public class ProductDTO {

    private Integer id;
    private String name;
    private String description;
    private Double price;
    private Brand brand;
    private SubCategory subCategory;
    private String categoryName;
    private String imageURL;

    public ProductDTO() {
    }

    public ProductDTO(final Product product) {
        id = product.getId();
        name = product.getName();
        price = product.getPrice();
        description = product.getDescription();
        brand = product.getBrand();
        subCategory = product.getSubCategory();
        imageURL = product.getProductImageURL();
        categoryName = product.getSubCategory().getCategory().getName();
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setPrice(final Double price) {
        this.price = price;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(final Brand brand) {
        this.brand = brand;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(final String categoryName) {
        this.categoryName = categoryName;
    }

    public void setImageURL(final String imageURL) {
        this.imageURL = imageURL;
    }
}
