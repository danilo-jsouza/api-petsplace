package apipetsplace.services;

import apipetsplace.domain.Brand;
import apipetsplace.dto.BrandDTO;

import java.util.List;

public interface BrandService {

    Brand findOne(Integer brandId);

    Brand insert(Brand brand);

    Brand fromDTO(BrandDTO brandDTO);

    Brand update(Brand brand);

    Boolean existsById(Integer brandId);

    List<Brand> findAll();

}
