package apipetsplace.services;

import apipetsplace.services.exceptions.FileException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

@Service
public class S3ServiceImpl implements S3Service {

    private Logger LOG = LoggerFactory.getLogger(S3Service.class);

    @Value("${s3.bucket}")
    private String bucketName;

    private final AmazonS3 amazonS3;

    public S3ServiceImpl(final AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    @Override
    public URI uploadFile(final MultipartFile multipartFile) throws IOException {
        try {
            String fileName = multipartFile.getOriginalFilename();
            InputStream inputStream = multipartFile.getInputStream();
            String contextType = multipartFile.getContentType();
            return uploadFile(inputStream, fileName, contextType);

        } catch (IOException | URISyntaxException e) {
            throw new FileException("IO Error: " + e.getMessage());
        }

    }

    private URI uploadFile(final InputStream inputStream, final String fileName,
                           final String contextType) throws URISyntaxException {
        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(contextType);

            LOG.info("Initialized upload");
            amazonS3.putObject(new PutObjectRequest(bucketName, fileName, inputStream, metadata));
            LOG.info("Concluded upload");

            return amazonS3.getUrl(bucketName, fileName).toURI();
        } catch (URISyntaxException e) {
            throw new FileException("Error URL to URI");
        }
    }


}
