package apipetsplace.services;

import apipetsplace.domain.SubCategory;
import apipetsplace.dto.SubCategoryDTO;

import java.util.List;

public interface SubCategoryService {

    SubCategory findOne(Integer subCategoryId);

    SubCategory insert(SubCategory SubCategory);

    SubCategory fromDTO(SubCategoryDTO SubCategoryDTO);

    SubCategory update(SubCategory SubCategory);

    Boolean existsById(Integer SubCategoryId);

    List<SubCategory> findAll();
}
