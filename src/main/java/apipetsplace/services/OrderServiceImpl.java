package apipetsplace.services;

import apipetsplace.domain.*;
import apipetsplace.domain.enums.Profile;
import apipetsplace.dto.OrderDTO;
import apipetsplace.repositories.OrderItemRepository;
import apipetsplace.repositories.OrderRepository;
import apipetsplace.security.UserSS;
import apipetsplace.services.exceptions.AuthorizationException;
import apipetsplace.services.exceptions.ObjectNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {


    private final OrderRepository orderRepository;
    private final ProductService productService;
    private final OrderItemRepository orderItemRepository;
    private final ConsumerService consumerService;

    public OrderServiceImpl(final OrderRepository orderRepository, final ProductService productService,
                            final OrderItemRepository orderItemRepository, final ConsumerService consumerService) {
        this.orderRepository = Objects.requireNonNull(orderRepository);
        this.productService = Objects.requireNonNull(productService);
        this.orderItemRepository = orderItemRepository;
        this.consumerService = Objects.requireNonNull(consumerService);
    }

    @Override
    public Order findOne(final Integer orderId) {
        final Optional<Order> order = orderRepository.findById(orderId);
        return order.orElseThrow(() -> new ObjectNotFoundException(
                "Object not found!" +
                        " id: " + orderId +
                        ", type class: " + Order.class.getName()));
    }

    @Override
    public Order insert(final Order order) {
        order.setId(null);
        Order orders = orderRepository.save(order);
        order.setId(orders.getId());
        for (OrderItem orderItem : order.getOrderItems()) {
            orderItem.setDiscount(0.0);
            orderItem.setPrice(productService.findOne(orderItem.getProduct().getId()).getPrice());
            orderItem.setOrder(order);
        }
        orderItemRepository.saveAll(order.getOrderItems());
        return orders;
    }

    @Override
    public Order fromDTO(final OrderDTO orderDTO) {
        final Consumer consumer = new Consumer(orderDTO.getConsumerId(), null, null, null);
        final Destiny destiny = new Destiny(orderDTO.getDestinyId(), null, null,
                null, null, null, null, consumer);
        final Order order = new Order(orderDTO.getId(), orderDTO.getDate(), consumer, destiny);
        order.setOrderItems(orderDTO.getOrderItems());
        return order;
    }

    @Override
    public Order update(final Order order) {
        if (order.getId() != null) {
            if (existsById(order.getId())) {
                return orderRepository.save(order);
            }
        }
        return null;
    }

    @Override
    public Boolean existsById(final Integer orderId) {
        final Boolean resultId = orderRepository.existsById(orderId);
        if (!resultId) {
            throw new ObjectNotFoundException(
                    "Object not found!" +
                            " id: " + orderId +
                            ", type class: " + Order.class.getName());
        } else {
            return resultId;
        }
    }


    @Override
    public Page<Order> searchOrders(final Integer page, final Integer linesPerPage,
                                    final String orderBy, final String direction) {
        final UserSS userSS = UserService.authenticated();

        if (userSS == null) {
            throw new AuthorizationException("Access denied");
        }

        final PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        if (userSS.hasRole(Profile.ADMIN)) {
            return orderRepository.findAll(pageRequest);
        } else {
            final Consumer consumer = consumerService.findOne(userSS.getId());
            return orderRepository.findByConsumer(consumer, pageRequest);
        }
    }

}
