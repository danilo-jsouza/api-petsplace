package apipetsplace.services;

import apipetsplace.domain.Order;
import apipetsplace.dto.OrderDTO;
import org.springframework.data.domain.Page;


public interface OrderService {

    Order findOne(Integer orderId);

    Order insert(Order order);

    Order fromDTO(OrderDTO orderDTO);

    Order update(Order order);

    Boolean existsById(Integer orderId);

    Page<Order> searchOrders(Integer page, Integer linesPerPage, String orderBy, String direction);

}
