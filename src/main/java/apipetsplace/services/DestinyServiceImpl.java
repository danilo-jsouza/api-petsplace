package apipetsplace.services;

import apipetsplace.domain.Consumer;
import apipetsplace.domain.Destiny;
import apipetsplace.dto.DestinyDTO;
import apipetsplace.repositories.DestinyRepository;
import apipetsplace.services.exceptions.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class DestinyServiceImpl implements DestinyService {


    private final DestinyRepository destinyRepository;

    public DestinyServiceImpl(final DestinyRepository destinyRepository) {
        this.destinyRepository = Objects.requireNonNull(destinyRepository);
    }

    @Override
    public Destiny findOne(final Integer destinyId) {
        final Optional<Destiny> destiny = destinyRepository.findById(destinyId);
        return destiny.orElseThrow(() -> new ObjectNotFoundException(
                "Object not found!" +
                        " id: " + destinyId +
                        ", type class: " + Destiny.class.getName()));
    }

    @Override
    public Destiny insert(final Destiny destiny) {
        destiny.setId(null);
        return destinyRepository.save(destiny);
    }

    @Override
    public Destiny fromDTO(final DestinyDTO destinyDTO) {
        final Consumer consumer = new Consumer(destinyDTO.getConsumerId(), null, null, null);
        final Destiny destiny = new Destiny(destinyDTO.getId(), destinyDTO.getAddress(), destinyDTO.getNumber(),
                destinyDTO.getComplement(), destinyDTO.getNeighborhood(), destinyDTO.getZipcode(), destinyDTO.getUf(), consumer);
        return destiny;
    }

    @Override
    public Destiny update(final Destiny destiny) {
        if (destiny.getId() != null) {
            if (existsById(destiny.getId())) {
                return destinyRepository.save(destiny);
            }
        }
        return null;
    }

    @Override
    public Boolean existsById(final Integer destinyId) {
        final Boolean resultId = destinyRepository.existsById(destinyId);
        if (!resultId) {
            throw new ObjectNotFoundException(
                    "Object not found!" +
                            " id: " + destinyId +
                            ", type class: " + Destiny.class.getName());
        } else {
            return resultId;
        }
    }

}
