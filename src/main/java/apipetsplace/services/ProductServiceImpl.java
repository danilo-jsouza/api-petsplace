package apipetsplace.services;

import apipetsplace.domain.Brand;
import apipetsplace.domain.Product;
import apipetsplace.domain.SubCategory;
import apipetsplace.dto.NewProductDTO;
import apipetsplace.dto.ProductDTO;
import apipetsplace.repositories.ProductRepository;
import apipetsplace.repositories.SubCategoryRepository;
import apipetsplace.services.exceptions.ObjectNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final SubCategoryRepository subCategoryRepository;
    private final S3Service s3Service;
    private final BrandService brandService;
    private final SubCategoryService subCategoryService;

    public ProductServiceImpl(final ProductRepository productRepository,
                              final SubCategoryRepository subCategoryRepository,
                              final S3Service s3Service,
                              final BrandService brandService,
                              final SubCategoryService subCategoryService) {
        this.productRepository = Objects.requireNonNull(productRepository);
        this.subCategoryRepository = Objects.requireNonNull(subCategoryRepository);
        this.s3Service = s3Service;
        this.brandService = Objects.requireNonNull(brandService);
        this.subCategoryService = Objects.requireNonNull(subCategoryService);
    }

    @Override
    public Product findOne(final Integer productId) {
        final Optional<Product> product = productRepository.findById(productId);
        return product.orElseThrow(() -> new ObjectNotFoundException(
                "Object not found!" +
                        " id: " + productId +
                        ", type class: " + Product.class.getName()));
    }

    @Override
    public Product insert(final Product product) {
        product.setId(null);
        return productRepository.save(product);
    }

    @Override
    public Product fromDTO(final ProductDTO productDTO) {
        final Brand brand = productDTO.getBrand();
        final SubCategory subCategory = new SubCategory(productDTO.getSubCategory().getId(), null, null);

        final Product product = new Product(productDTO.getId(), productDTO.getName(),
                productDTO.getPrice(), productDTO.getDescription(), brand, subCategory, productDTO.getImageURL());
        final String categoryName = product.getSubCategory().getCategory().getName();
        return product;
    }

    @Override
    public Product fromDTO(final NewProductDTO productDTO) {
        final Integer brandId = Integer.parseInt(productDTO.getBrandId());
        final Integer subCategoryId = Integer.parseInt(productDTO.getSubCategoryId());
        validationDTO(brandId, subCategoryId);

        final Brand brand = new Brand(brandId, null);
        final SubCategory subCategory = new SubCategory(subCategoryId, null, null);
        final Product product = new Product(productDTO.getId(), productDTO.getName(), Double.parseDouble(productDTO.getPrice()),
                productDTO.getDescription(), brand, subCategory, productDTO.getImageURL());
        return product;
    }

    @Override
    public Product update(final Product product) {
        if (product.getId() != null) {
            if (existsById(product.getId())) {
                return productRepository.save(product);
            }
        }
        return null;
    }

    @Override
    public Boolean existsById(final Integer productId) {
        final Boolean resultId = productRepository.existsById(productId);
        if (!resultId) {
            throw new ObjectNotFoundException(
                    "Object not found!" +
                            " id: " + productId +
                            ", type class: " + Product.class.getName());
        } else {
            return resultId;
        }
    }

    @Override
    public Page<Product> searchProductByCategory(final Integer id, final Integer page, final Integer linesPerPage,
                                                 final String orderBy, final String direction) {
        final PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);

        return productRepository.searchProductByCategories(id, pageRequest);
    }

    @Override
    public Page<Product> searchProductBySubCategoriesId(final List<Integer> ids, final Integer page,
                                                        final Integer linesPerPage, final String orderBy,
                                                        final String direction) {
        final PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);

        final List<SubCategory> subCategories = subCategoryRepository.findAllById(ids);
        return productRepository.searchProductBySubCategories(subCategories, pageRequest);
    }

    @Override
    public Page<Product> searchProductByName(final String productName, final Integer page, final Integer linesPerPage,
                                             final String orderBy, final String direction) {
        final PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return productRepository.searchProductByName(productName, pageRequest);
    }

    @Override
    public Page<Product> searchProduct(final Integer page, final Integer linesPerPage,
                                       final String orderBy, final String direction) {
        PageRequest pageRequest = PageRequest.of(page, linesPerPage);
        return productRepository.findAll(pageRequest);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAllByOrderByIdAsc();
    }

    @Override
    public URI uploadProfilePicture(final MultipartFile multipartFile, final Integer productId) throws IOException {
        final URI uri = s3Service.uploadFile(multipartFile);
        Product product = findOne(productId);
        product.setProductImageURL(uri.toString());
        update(product);

        return uri;
    }

    @Override
    public void validationDTO(final Integer brandId, final Integer subCategoryId) {
        brandService.existsById(brandId);
        subCategoryService.existsById(subCategoryId);
    }

}
