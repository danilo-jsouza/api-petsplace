package apipetsplace.services;

import apipetsplace.domain.Consumer;
import apipetsplace.repositories.ConsumerRepository;
import apipetsplace.security.UserSS;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final ConsumerRepository consumerRepository;

    public UserDetailsServiceImpl(final ConsumerRepository consumerRepository) {
        this.consumerRepository = Objects.requireNonNull(consumerRepository);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Consumer consumer = consumerRepository.findByEmail(email);
        if (consumer == null) {
            throw new UsernameNotFoundException(email);
        }

        return new UserSS(consumer.getId(), consumer.getEmail(), consumer.getPassword(), consumer.getProfiles());
    }
}
