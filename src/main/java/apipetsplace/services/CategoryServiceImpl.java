package apipetsplace.services;


import apipetsplace.domain.Category;
import apipetsplace.domain.SubCategory;
import apipetsplace.dto.CategoryDTO;
import apipetsplace.repositories.CategoryRepository;
import apipetsplace.services.exceptions.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(final CategoryRepository categoryRepository) {
        this.categoryRepository = Objects.requireNonNull(categoryRepository);
    }

    @Override
    public Category findOne(final Integer categoryId) {
        final Optional<Category> category = categoryRepository.findById(categoryId);
        return category.orElseThrow(() -> new ObjectNotFoundException(
                "Object not found!" +
                        " id: " + categoryId +
                        ", type class: " + Category.class.getName()));
    }

    @Override
    public Category insert(final Category category) {
        category.setId(null);
        return categoryRepository.save(category);
    }

    @Override
    public Category fromDTO(final CategoryDTO categoryDTO) {
        final Category category = new Category(categoryDTO.getId(), categoryDTO.getName());
        final List<SubCategory> subCategories = categoryDTO.getSubCategories();
        category.setSubCategory(subCategories);
        return category;
    }

    @Override
    public Category update(final Category category) {
        if (category.getId() != null) {
            if (existsById(category.getId())) {
                return categoryRepository.save(category);
            }
        }
        return null;
    }

    @Override
    public Boolean existsById(final Integer categoryId) {
        final Boolean resultId = categoryRepository.existsById(categoryId);
        if (!resultId) {
            throw new ObjectNotFoundException(
                    "Object not found!" +
                            " id: " + categoryId +
                            ", type class: " + Category.class.getName());
        } else {
            return resultId;
        }
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

}
