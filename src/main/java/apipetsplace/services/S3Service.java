package apipetsplace.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;

public interface S3Service {

    URI uploadFile(MultipartFile multipartFile) throws IOException;

}
