package apipetsplace.services;

import apipetsplace.domain.Consumer;
import apipetsplace.dto.ConsumerDTO;

public interface ConsumerService {

    Consumer findOne(Integer consumerId);

    Consumer insert(Consumer consumer);

    Consumer fromDTO(ConsumerDTO consumerDTO);

    Consumer update(Consumer consumer);

    Boolean existsById(Integer consumerId);

}
