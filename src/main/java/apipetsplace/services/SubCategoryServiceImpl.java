package apipetsplace.services;

import apipetsplace.domain.Category;
import apipetsplace.domain.SubCategory;
import apipetsplace.dto.SubCategoryDTO;
import apipetsplace.repositories.SubCategoryRepository;
import apipetsplace.services.exceptions.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class SubCategoryServiceImpl implements SubCategoryService {

    private final SubCategoryRepository subCategoryRepository;

    public SubCategoryServiceImpl(final SubCategoryRepository subCategoryRepository) {
        this.subCategoryRepository = Objects.requireNonNull(subCategoryRepository);
    }

    @Override
    public SubCategory findOne(final Integer subCategoryId) {
        final Optional<SubCategory> subCategory = subCategoryRepository.findById(subCategoryId);
        return subCategory.orElseThrow(() -> new ObjectNotFoundException(
                "Object not found!" +
                        " id: " + subCategoryId +
                        ", type class: " + SubCategory.class.getName()));
    }

    @Override
    public SubCategory insert(final SubCategory subCategory) {
        subCategory.setId(null);
        return subCategoryRepository.save(subCategory);
    }

    @Override
    public SubCategory fromDTO(final SubCategoryDTO subCategoryDTO) {
        final Category category = new Category(subCategoryDTO.getCategoryId(), null);
        final SubCategory subCategory = new SubCategory(subCategoryDTO.getId(), subCategoryDTO.getName(), category);
        return subCategory;
    }

    @Override
    public SubCategory update(final SubCategory subCategory) {
        if (subCategory.getId() != null) {
            if (existsById(subCategory.getId())) {
                return subCategoryRepository.save(subCategory);
            }
        }
        return null;
    }

    @Override
    public Boolean existsById(final Integer subCategoryId) {
        final Boolean resultId = subCategoryRepository.existsById(subCategoryId);
        if (!resultId) {
            throw new ObjectNotFoundException(
                    "Object not found!" +
                            " id: " + subCategoryId +
                            ", type class: " + SubCategory.class.getName());
        } else {
            return resultId;
        }
    }

    @Override
    public List<SubCategory> findAll() {
        return subCategoryRepository.findAll();
    }

}
