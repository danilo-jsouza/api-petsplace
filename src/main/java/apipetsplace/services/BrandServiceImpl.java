package apipetsplace.services;

import apipetsplace.domain.Brand;
import apipetsplace.domain.Product;
import apipetsplace.dto.BrandDTO;
import apipetsplace.repositories.BrandRepository;
import apipetsplace.services.exceptions.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepository brandRepository;

    public BrandServiceImpl(final BrandRepository brandRepository) {
        this.brandRepository = Objects.requireNonNull(brandRepository);
    }

    @Override
    public Brand findOne(final Integer brandId) {
        final Optional<Brand> brand = brandRepository.findById(brandId);
        return brand.orElseThrow(() -> new ObjectNotFoundException(
                "Object not found!" +
                        " id: " + brandId +
                        ", type class: " + Brand.class.getName()));
    }

    @Override
    public Brand insert(final Brand brand) {
        brand.setId(null);
        return brandRepository.save(brand);
    }

    @Override
    public Brand fromDTO(final BrandDTO brandDTO) {
        final Brand brand = new Brand(brandDTO.getId(), brandDTO.getName());
        return brand;
    }

    @Override
    public Brand update(final Brand brand) {
        if (brand.getId() != null) {
            if (existsById(brand.getId())) {
                return brandRepository.save(brand);
            }
        }
        return null;
    }

    @Override
    public List<Brand> findAll() {
        return brandRepository.findAll();
    }

    @Override
    public Boolean existsById(final Integer brandId) {
        final Boolean resultId = brandRepository.existsById(brandId);
        if (!resultId) {
            throw new ObjectNotFoundException(
                    "Object not found!" +
                            " id: " + brandId +
                            ", type class: " + Brand.class.getName());
        } else {
            return resultId;
        }
    }

}
