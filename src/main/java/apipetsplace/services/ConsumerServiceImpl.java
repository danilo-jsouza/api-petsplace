package apipetsplace.services;

import apipetsplace.domain.Consumer;
import apipetsplace.domain.Destiny;
import apipetsplace.domain.enums.Profile;
import apipetsplace.dto.ConsumerDTO;
import apipetsplace.repositories.ConsumerRepository;
import apipetsplace.security.UserSS;
import apipetsplace.services.exceptions.AuthorizationException;
import apipetsplace.services.exceptions.ObjectNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ConsumerServiceImpl implements ConsumerService {

    private final ConsumerRepository consumerRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public ConsumerServiceImpl(final ConsumerRepository consumerRepository, final BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.consumerRepository = Objects.requireNonNull(consumerRepository);
        this.bCryptPasswordEncoder = Objects.requireNonNull(bCryptPasswordEncoder);
    }

    @Override
    public Consumer findOne(final Integer consumerId) {

        UserSS userSS = UserService.authenticated();

        final Optional<Consumer> consumer = consumerRepository.findById(consumerId);
        if (userSS.getId().equals(consumerId)) {
            return consumer.orElseThrow(() -> new ObjectNotFoundException(
                    "Object not found!" +
                            " id: " + consumerId +
                            ", type class: " + Consumer.class.getName()));

        } else {
            throw new AuthorizationException("Access denied");
        }
    }

    @Override
    public Consumer insert(final Consumer consumer) {
        consumer.setId(null);
        return consumerRepository.save(consumer);
    }

    @Override
    public Consumer fromDTO(final ConsumerDTO consumerDTO) {
        final List<Destiny> destinies = consumerDTO.getDestinies();
        Consumer consumer = new Consumer(consumerDTO.getId(),
                consumerDTO.getName(),
                consumerDTO.getEmail(),
                bCryptPasswordEncoder.encode(consumerDTO.getPassword()));
        if (!destinies.isEmpty()) {
            consumer.setDestinies(destinies);
        }
        return consumer;
    }

    @Override
    public Consumer update(final Consumer consumer) {
        if (consumer.getId() != null) {
            if (existsById(consumer.getId())) {
                return consumerRepository.save(consumer);
            }
        }
        return null;
    }

    @Override
    public Boolean existsById(final Integer consumerId) {
        final Boolean resultId = consumerRepository.existsById(consumerId);
        if (!resultId) {
            throw new ObjectNotFoundException(
                    "Object not found!" +
                            " id: " + consumerId +
                            ", type class: " + Consumer.class.getName());
        } else {
            return resultId;
        }
    }

}
