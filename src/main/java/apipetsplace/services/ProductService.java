package apipetsplace.services;

import apipetsplace.domain.Product;
import apipetsplace.dto.NewProductDTO;
import apipetsplace.dto.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.util.List;

public interface ProductService {

    Product findOne(Integer productId);

    Product insert(Product product);

    Product fromDTO(ProductDTO productDTO);

    Product fromDTO(NewProductDTO newProductDTO);

    Product update(Product product);

    Boolean existsById(Integer productId);

    Page<Product> searchProductByCategory(Integer ids, Integer page, Integer linesPerPage, String orderBy, String direction);

    Page<Product> searchProductBySubCategoriesId(List<Integer> ids, Integer page, Integer linesPerPage, String orderBy, String direction);

    Page<Product> searchProductByName(String productName, Integer page, Integer linesPerPage, String orderBy, String direction);

    Page<Product> searchProduct(Integer page, Integer linesPerPage, String orderBy, String direction);

    List<Product> findAll();

    URI uploadProfilePicture(MultipartFile multipartFile, Integer productId) throws IOException;

    void validationDTO(Integer brandId, Integer subCategoryId);

}
