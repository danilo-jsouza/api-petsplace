package apipetsplace.services;

import apipetsplace.domain.Category;
import apipetsplace.dto.CategoryDTO;

import java.util.List;

public interface CategoryService {

    Category findOne(Integer categoryId);

    Category insert(Category category);

    Category fromDTO(CategoryDTO categoryDTO);

    Category update(Category category);

    Boolean existsById(Integer categoryId);

    List<Category> findAll();
}
