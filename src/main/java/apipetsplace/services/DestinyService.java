package apipetsplace.services;

import apipetsplace.domain.Destiny;
import apipetsplace.dto.DestinyDTO;

public interface DestinyService {

    Destiny findOne(Integer DestinyId);

    Destiny insert(Destiny Destiny);

    Destiny fromDTO(DestinyDTO DestinyDTO);

    Destiny update(Destiny Destiny);

    Boolean existsById(Integer DestinyId);

}
