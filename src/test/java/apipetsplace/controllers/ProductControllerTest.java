package apipetsplace.controllers;


import apipetsplace.domain.Product;
import apipetsplace.dto.ProductDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductControllerTest extends AbstractControllerTest {

    @Test
    public void testGetProductById() throws Exception {
        //given
        final ProductDTO product = createProductDTOController("Sabores clandestinos", "Everyone");

        //when
        final Product productResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_PRODUCT + "/" + product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), Product.class);

        //then
        Assertions.assertEquals(product.getId(), productResponse.getId());
        Assertions.assertEquals(product.getName(), productResponse.getName());
    }

    @Test
    public void testExceptionShouldBeThrownWhenIdNotFound() throws Exception {
        MockHttpServletResponse mockClientHttpResponse = mvc.perform(MockMvcRequestBuilders
                .get(API_PRODUCT + "/" + 100)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn().getResponse();
    }

    @Test
    public void testCreateNewProduct() throws Exception {
        final ProductDTO productDTO = createProductDTOController("Opala racao", "EveryOne");

        mvc.perform(MockMvcRequestBuilders
                .post(API_PRODUCT)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(productDTO))).andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testShouldChangeProductName() throws Exception {
        ProductDTO product = createProductDTOController("Bres", "everyOne");
        product.setId(product.getId());
        product.setName("Pedigre");

        mvc.perform(MockMvcRequestBuilders
                .put(API_PRODUCT + "/" + product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(product))).andExpect(status().is2xxSuccessful());

        final Product productResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_PRODUCT + "/" + product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(product))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), Product.class);

        Assertions.assertEquals("Pedigre", productResponse.getName());
    }

}
