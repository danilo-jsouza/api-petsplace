package apipetsplace.controllers;

import apipetsplace.domain.Consumer;
import apipetsplace.dto.ConsumerDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ConsumerControllerTest extends AbstractControllerTest {


    @Test
    public void testGetConsumerById() throws Exception {
        //given
        final ConsumerDTO consumer = createConsumerDTOController("matheus.dias@maders.com.br");


        //when
        mvc.perform(MockMvcRequestBuilders
                .get(API_CONSUMER + "/" + consumer.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("email").value(consumer.getEmail()))
                .andExpect(jsonPath("id").value(consumer.getId()));
    }

    @Test
    public void testExceptionShouldBeThrownWhenIdNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get(API_CONSUMER + "/" + 100)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn().getResponse();
    }

    @Test
    public void testCreateNewConsumer() throws Exception {
        final ConsumerDTO consumerDTO = new ConsumerDTO();
        consumerDTO.setName("Matheus");
        consumerDTO.setEmail("matheus_jeremias@dot.com");
        consumerDTO.setPassword(bCryptPasswordEncoder.encode("123"));

        mvc.perform(MockMvcRequestBuilders
                .post(API_CONSUMER)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(consumerDTO))).andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testShouldChangeConsumerName() throws Exception {
        ConsumerDTO consumer = createConsumerDTOController("csur_consmer@consumer.com.br");
        consumer.setId(consumer.getId());
        consumer.setEmail("consur.consumerteste@cnsumer.com.br");

        String json = new ObjectMapper().writeValueAsString(consumer);

        final MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(API_CONSUMER + "/" + consumer.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json);

        mvc.perform(requestBuilder)
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("email").value(consumer.getEmail()));
    }

}
