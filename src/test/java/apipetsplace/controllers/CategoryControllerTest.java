package apipetsplace.controllers;

import apipetsplace.domain.Category;
import apipetsplace.dto.CategoryDTO;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class CategoryControllerTest extends AbstractControllerTest {

    @Test
    public void testGetCategoryById() throws Exception {
        //given
        final CategoryDTO category = createCategoryDTOController("Gatos");

        //when
        final Category categoryResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_CATEGORIES + "/" + category.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), Category.class);

        //then
        Assertions.assertEquals(category.getId(), categoryResponse.getId());
        Assertions.assertEquals(category.getName(), categoryResponse.getName());
    }

    @Test
    public void testExceptionShouldBeThrownWhenIdNotFound() throws Exception {
        MockHttpServletResponse mockClientHttpResponse = mvc.perform(MockMvcRequestBuilders
                .get(API_CATEGORIES + "/" + 100)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andReturn().getResponse();
    }

    @Test
    public void testCreateNewCategory() throws Exception {
        final CategoryDTO categoryDTO = createCategoryDTOController("Ração Caseira");

        mvc.perform(MockMvcRequestBuilders
                .post(API_CATEGORIES)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(categoryDTO))).andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testShouldChangeCategoryName() throws Exception {
        CategoryDTO category = createCategoryDTOController("Marcas");
        category.setId(category.getId());
        category.setName("Ratos de Gado");

        mvc.perform(MockMvcRequestBuilders
                .put(API_CATEGORIES + "/" + category.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(category))).andExpect(status().is2xxSuccessful());

        final Category categoryResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_CATEGORIES + "/" + category.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(category))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), Category.class);

        Assertions.assertEquals("Ratos de Gado", categoryResponse.getName());
    }

}

