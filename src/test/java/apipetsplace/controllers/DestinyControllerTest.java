package apipetsplace.controllers;

import apipetsplace.domain.Destiny;
import apipetsplace.dto.DestinyDTO;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DestinyControllerTest extends AbstractControllerTest {

    @Test
    public void testGetDestinyById() throws Exception {
        //given
        final DestinyDTO destiny = createDestinyDTOController("Rua dos Tres irmaos", "teste@dominio.br");
        //when
        final Destiny destinyResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_DESTINY + "/" + destiny.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), Destiny.class);

        //then
        Assertions.assertEquals(destiny.getId(), destinyResponse.getId());
        Assertions.assertEquals(destiny.getAddress(), destinyResponse.getAddress());
    }

    @Test
    public void testExceptionShouldBeThrownWhenIdNotFound() throws Exception {
        MockHttpServletResponse mockClientHttpResponse = mvc.perform(MockMvcRequestBuilders
                .get(API_DESTINY + "/" + 100)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andReturn().getResponse();
    }

    @Test
    public void testCreateNewDestiny() throws Exception {
        final DestinyDTO destinyDTO = createDestinyDTOController("Rua dos Tres irmaos", "consumer@consumer.com");

        mvc.perform(MockMvcRequestBuilders
                .post(API_DESTINY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(destinyDTO))).andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testShouldChangeDestinyName() throws Exception {
        DestinyDTO destiny = createDestinyDTOController("Rua dos Tres irmaos", "consumer.consumer@consumer.com.br");
        destiny.setId(destiny.getId());
        destiny.setAddress("Rua dos Tres mosqueteiros");

        mvc.perform(MockMvcRequestBuilders
                .put(API_DESTINY + "/" + destiny.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(destiny))).andExpect(status().is2xxSuccessful());

        final Destiny destinyResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_DESTINY + "/" + destiny.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(destiny))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), Destiny.class);

        Assertions.assertEquals("Rua dos Tres mosqueteiros", destinyResponse.getAddress());
    }


}
