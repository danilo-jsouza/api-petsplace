package apipetsplace.controllers;

import apipetsplace.domain.Brand;
import apipetsplace.dto.BrandDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BrandControllerTest extends AbstractControllerTest {

    @Test
    public void testGetBrandById() throws Exception {
        //given
        final BrandDTO brand = createBrandDTOController("Sabores clandestinos");

        //when
        final Brand brandResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_BRAND + "/" + brand.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), Brand.class);

        //then
        Assertions.assertEquals(brand.getId(), brandResponse.getId());
        Assertions.assertEquals(brand.getName(), brandResponse.getName());
    }

    @Test
    public void testExceptionShouldBeThrownWhenIdNotFound() throws Exception {
        MockHttpServletResponse mockClientHttpResponse = mvc.perform(MockMvcRequestBuilders
                .get(API_BRAND + "/" + 100)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn().getResponse();
    }

    @Test
    public void testCreateNewBrand() throws Exception {
        final BrandDTO brandDTO = createBrandDTOController("Opala racao");

        mvc.perform(MockMvcRequestBuilders
                .post(API_BRAND)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(brandDTO))).andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testShouldChangeBrandName() throws Exception {
        BrandDTO brand = createBrandDTOController("Bres");
        brand.setId(brand.getId());
        brand.setName("Pedigre");

        mvc.perform(MockMvcRequestBuilders
                .put(API_BRAND + "/" + brand.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(brand))).andExpect(status().is2xxSuccessful());

        final Brand brandResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_BRAND + "/" + brand.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(brand))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), Brand.class);

        Assertions.assertEquals("Pedigre", brandResponse.getName());
    }

}
