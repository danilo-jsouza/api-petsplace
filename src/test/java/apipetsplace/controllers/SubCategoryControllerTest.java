package apipetsplace.controllers;

import apipetsplace.domain.SubCategory;
import apipetsplace.dto.SubCategoryDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SubCategoryControllerTest extends AbstractControllerTest {

    @Test
    public void testGetSubCategoryById() throws Exception {
        //given
        final SubCategoryDTO subCategory = createSubCategoryDTOController("Gatos");

        //when
        final SubCategory subCategoryResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_SUB_CATEGORIES + "/" + subCategory.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), SubCategory.class);

        //then
        Assertions.assertEquals(subCategory.getId(), subCategoryResponse.getId());
        Assertions.assertEquals(subCategory.getName(), subCategoryResponse.getName());
    }

    @Test
    public void testExceptionShouldBeThrownWhenIdNotFound() throws Exception {
        MockHttpServletResponse mockClientHttpResponse = mvc.perform(MockMvcRequestBuilders
                .get(API_SUB_CATEGORIES + "/" + 100)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn().getResponse();
    }

    @Test
    public void testCreateNewSubCategory() throws Exception {
        final SubCategoryDTO subCategoryDTO = createSubCategoryDTOController("Ração Caseira");

        mvc.perform(MockMvcRequestBuilders
                .post(API_CATEGORIES)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(subCategoryDTO))).andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testShouldChangeSubCategoryName() throws Exception {
        SubCategoryDTO subCategoryDTO = createSubCategoryDTOController("Ração do Matheus");
        subCategoryDTO.setName("Ratos de Gado");

        mvc.perform(MockMvcRequestBuilders
                .put(API_SUB_CATEGORIES + "/" + subCategoryDTO.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(subCategoryDTO))).andExpect(status().is2xxSuccessful());

        final SubCategory subCategoryResponse = jsonConverter.toObject(mvc.perform(MockMvcRequestBuilders
                .get(API_SUB_CATEGORIES + "/" + subCategoryDTO.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(subCategoryDTO))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString(), SubCategory.class);

        Assertions.assertEquals("Ratos de Gado", subCategoryResponse.getName());
    }

}
