package apipetsplace.controllers;

import apipetsplace.AbstractTest;
import apipetsplace.domain.*;
import apipetsplace.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;

public abstract class AbstractControllerTest extends AbstractTest {

    @Autowired
    protected MockMvc mvc;

    protected static final String API_CATEGORIES = "/api/categories";
    protected static final String API_SUB_CATEGORIES = "/api/categories/sub";
    protected static final String API_DESTINY = "/api/destinies";
    protected static final String API_CONSUMER = "/api/consumers";
    protected static final String API_BRAND = "/api/products/brand";
    protected static final String API_PRODUCT = "/api/products";
    protected static final String API_ORDER = "/api/orders";

    protected CategoryDTO createCategoryDTOController(final String name) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName(name);
        Category category = categoryService.fromDTO(categoryDTO);
        categoryService.insert(category);
        categoryDTO.setId(category.getId());
        return categoryDTO;
    }

    protected SubCategoryDTO createSubCategoryDTOController(final String name) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName(name);
        Category category = categoryService.fromDTO(categoryDTO);
        categoryService.insert(category);
        categoryDTO.setId(category.getId());
        SubCategoryDTO subCategoryDTO = new SubCategoryDTO();
        subCategoryDTO.setName(name);
        subCategoryDTO.setCategoryId(category.getId());
        SubCategory subCategory = subCategoryService.fromDTO(subCategoryDTO);
        subCategoryService.insert(subCategory);
        subCategoryDTO.setId(subCategory.getId());
        return subCategoryDTO;
    }


    protected DestinyDTO createDestinyDTOController(final String address, final String consumerEmail) {
        return new DestinyDTO();
    }

    protected ConsumerDTO createConsumerDTOController(final String email) {
        ConsumerDTO consumerDTO = new ConsumerDTO();
        consumerDTO.setName("Maria Pessoa");
        consumerDTO.setEmail(email);
        consumerDTO.setPassword(bCryptPasswordEncoder.encode("matheus"));
        Consumer consumer = consumerService.fromDTO(consumerDTO);
        consumerService.insert(consumer);
        consumerDTO.setId(consumer.getId());
        return consumerDTO;
    }

    protected BrandDTO createBrandDTOController(final String name) {
        BrandDTO brandDTO = new BrandDTO();
        brandDTO.setName(name);
        Brand brand = brandService.fromDTO(brandDTO);
        brandService.insert(brand);
        brandDTO.setId(brand.getId());
        return brandDTO;
    }

    protected ProductDTO createProductDTOController(final String productName, final String subCategoryName) {
        BrandDTO brand = createBrandDTOController("Never Used this");
        ProductDTO productDTO = new ProductDTO();
        productDTO.setName(productName);
        productDTO.setDescription("Esssa é uma ração de cachorro famosa por ser famosa.");
        productDTO.setPrice(20.0);
        productDTO.setImageURL("https://");
        productDTO.getBrand().setId(1);
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName("Cachorro");
        Category category = categoryService.fromDTO(categoryDTO);
        categoryService.insert(category);
        categoryDTO.setId(category.getId());
        SubCategoryDTO subCategoryDTO = new SubCategoryDTO();
        subCategoryDTO.setName(subCategoryName);
        subCategoryDTO.setCategoryId(category.getId());
        SubCategory subCategory = subCategoryService.fromDTO(subCategoryDTO);
        subCategoryService.insert(subCategory);
        subCategoryDTO.setId(subCategory.getId());
        productDTO.setSubCategory(subCategory);


        Product product = productService.fromDTO(productDTO);

        productService.insert(product);
        productDTO.setId(product.getId());
        return productDTO;
    }

    protected OrderDTO createOrderDTOController(final String consumerEmail) {
        DestinyDTO destiny = createDestinyDTOController("Street on the bar", consumerEmail.toString());
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setConsumerId(destiny.getConsumerId());
        orderDTO.setDestinyId(destiny.getId());
        orderDTO.setDate(Instant.now());
        Order order = orderService.fromDTO(orderDTO);
        orderService.insert(order);
        orderDTO.setId(order.getId());
        return orderDTO;
    }

}
