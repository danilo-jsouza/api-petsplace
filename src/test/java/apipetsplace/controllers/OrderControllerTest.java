package apipetsplace.controllers;

import apipetsplace.domain.Order;
import apipetsplace.dto.OrderDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrderControllerTest extends AbstractControllerTest {


    @Test
    public void testGetOrderById() throws Exception {
        //given
        final OrderDTO order = createOrderDTOController("domn@io.cmo.br");
        //when
        mvc.perform(MockMvcRequestBuilders
                .get(API_ORDER + "/" + order.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id").value(order.getId()))
                .andExpect(jsonPath("createdDate").value(order.getDate().toString()));
    }

    @Test
    public void testExceptionShouldBeThrownWhenIdNotFound() throws Exception {
        MockHttpServletResponse mockClientHttpResponse = mvc.perform(MockMvcRequestBuilders
                .get(API_ORDER + "/" + 100)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn().getResponse();
    }

    @Test
    public void testCreateNewOrder() throws Exception {
        final OrderDTO orderDTO = createOrderDTOController("testcreateneworder@email.com");

        mvc.perform(MockMvcRequestBuilders
                .post(API_ORDER)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonConverter.toJson(orderDTO))).andExpect(status().is2xxSuccessful());
    }

}
