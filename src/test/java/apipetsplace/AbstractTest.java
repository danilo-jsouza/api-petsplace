package apipetsplace;


import apipetsplace.domain.Category;
import apipetsplace.repositories.*;
import apipetsplace.services.*;
import apipetsplace.utils.JsonConverter;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@SpringBootTest
@AutoConfigureMockMvc
public class AbstractTest {

    @Autowired
    protected JsonConverter jsonConverter;

    @Autowired
    protected CategoryRepository categoryRepository = Mockito.mock(CategoryRepository.class);

    @Autowired
    protected CategoryServiceImpl categoryService = new CategoryServiceImpl(categoryRepository);

    @Autowired
    protected SubCategoryRepository subCategoryRepository = Mockito.mock(SubCategoryRepository.class);

    @Autowired
    protected SubCategoryServiceImpl subCategoryService = new SubCategoryServiceImpl(subCategoryRepository);

    @Autowired
    protected DestinyRepository destinyRepository = Mockito.mock(DestinyRepository.class);

    @Autowired
    protected DestinyServiceImpl destinyService = new DestinyServiceImpl(destinyRepository);

    @Autowired
    protected ConsumerRepository consumerRepository = Mockito.mock(ConsumerRepository.class);

    @Autowired
    protected BCryptPasswordEncoder bCryptPasswordEncoder = Mockito.mock(BCryptPasswordEncoder.class);

    @Autowired
    protected ConsumerServiceImpl consumerService = new ConsumerServiceImpl(consumerRepository, bCryptPasswordEncoder);

    @Autowired
    protected BrandRepository brandRepository = Mockito.mock(BrandRepository.class);

    @Autowired
    protected BrandServiceImpl brandService = new BrandServiceImpl(brandRepository);

    @Autowired
    protected ProductRepository productRepository = Mockito.mock(ProductRepository.class);

    @Autowired
    protected  S3Service s3Service;

    @Autowired
    protected ProductServiceImpl productService = new ProductServiceImpl(productRepository, subCategoryRepository, s3Service, brandService, subCategoryService);

    @Autowired
    protected OrderRepository orderRepository = Mockito.mock(OrderRepository.class);

    @Autowired
    protected OrderItemRepository orderItemRepository;

    @Autowired
    protected OrderServiceImpl orderService = new OrderServiceImpl(orderRepository, productService, orderItemRepository, consumerService);

    public AbstractTest() {
    }

    public Category createCategory() {
        return new Category(1, "Ração");
    }
}
