package apipetsplace.services;

import apipetsplace.domain.Category;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.*;

import java.util.Optional;

public class CategoryServiceTest extends AbstractServiceTest {

    @Test
    public void getCategoryById() {
        //given
        Category category = createCategory();
        Mockito.when(categoryRepository.findById(category.getId())).thenReturn(Optional.of(category));

        //when
        Optional<Category> result = Optional.ofNullable(categoryService.findOne(category.getId()));

        //then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getId()).isEqualTo(category.getId());
        assertThat(result.get().getName()).isEqualTo(category.getName());
    }
}
