package apipetsplace.services;

import apipetsplace.AbstractTest;
import apipetsplace.repositories.CategoryRepository;
import org.mockito.Mockito;

public abstract class AbstractServiceTest extends AbstractTest {

    CategoryRepository categoryRepository = Mockito.mock(CategoryRepository.class);
    CategoryServiceImpl categoryService = new CategoryServiceImpl(categoryRepository);

}
