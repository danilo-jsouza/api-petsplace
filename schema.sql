CREATE TABLE category(
    id serial PRIMARY KEY NOT NULL,
    name VARCHAR(80)
);

CREATE TABLE sub_category(
    id serial PRIMARY KEY NOT NULL,
    name VARCHAR(80),
    category_id int NOT NULL,
        CONSTRAINT sub_category_category_id_fk FOREIGN KEY (category_id)
        REFERENCES category (id) MATCH SIMPLE
);

CREATE TABLE consumer(
    id serial PRIMARY KEY NOT NULL,
    name varchar (200),
    email varchar(100) unique,
    password varchar(200)
);

CREATE TABLE profiles(
    id serial PRIMARY KEY NOT NULL,
    profiles int not null,
    consumer_id int NOT NULL,
    CONSTRAINT profiles_consumer_id_fk FOREIGN KEY (consumer_id)
        REFERENCES consumer (id) MATCH SIMPLE
);

CREATE TABLE destiny(
    id serial PRIMARY KEY NOT NULL,
    address varchar(200),
    number varchar(10),
    complement varchar(100),
    neighborhood varchar(100),
    zipcode varchar(11),
    uf varchar(2),
    consumer_id int NOT NULL,
    CONSTRAINT destiny_consumer_id_fk FOREIGN KEY (consumer_id)
        REFERENCES consumer (id) MATCH SIMPLE
);

CREATE TABLE brand(
    id serial PRIMARY KEY NOT NULL,
    name varchar(100)
);

CREATE TABLE product(
    id serial PRIMARY KEY NOT NULL,
    name varchar(200),
    price double precision,
    description varchar(200),
    product_imageurl varchar(200),
    brand_id int NOT NULL,
    sub_category_id int NOT NULL,
    CONSTRAINT product_brand_id_fk FOREIGN KEY (brand_id)
            REFERENCES brand (id) MATCH SIMPLE,
    CONSTRAINT product_sub_category_id_fk FOREIGN KEY (sub_category_id)
            REFERENCES sub_category (id) MATCH SIMPLE
);

CREATE TABLE orders(
    id serial PRIMARY KEY NOT NULL,
    created_date timestamp,
    consumer_id int NOT NULL,
    destiny_id int NOT NULL,
    CONSTRAINT orders_consumer_id_fk FOREIGN KEY (consumer_id)
        REFERENCES consumer (id) MATCH SIMPLE,
    CONSTRAINT orders_destiny_id_fk FOREIGN KEY (destiny_id)
        REFERENCES destiny (id) MATCH SIMPLE
);

CREATE TABLE order_item(
    discount double precision, -- TODO Chave primária do order_item é order_id + product
    quantity int,
    price double precision,
    order_id int NOT NULL,
    product_id int NOT NULL,
    CONSTRAINT order_item_order_id_fk FOREIGN KEY (order_id)
        REFERENCES orders (id) MATCH SIMPLE,
    CONSTRAINT order_item_product_id_fk FOREIGN KEY (product_id)
        REFERENCES product (id) MATCH SIMPLE
);
