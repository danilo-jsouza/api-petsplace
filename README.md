# api-petsplace

## What does it do?
This service do...

## Responsibility
This service is responsible for ....


## Development

#### Pre-requisites:
* Java 11
* Docker
* If you want to use [Docker](https://docs.docker.com/), install version 1.12
or greater:
```
https://docs.docker.com/get-docker/
```


# How to run
To run the application make sure you have both `docker` and `docker-compose` installed on your machine.

Run the docker-compose.yml with the services needed (e.g postgres database):
```bash
docker-compose run
```

Then run the application main.
If everything's working you should see the following response 
when performing a GET request to http://localhost:8080/actuator/health:
```
{"status":"UP"}
```

##Try connection with a database PostgreSQL (docker)

To get a connection to the database you need to have the docker
running on your local machine and have installed postgres.
```
$ psql -h localhost -p 5432 -d postgres -U postgres --password
```
